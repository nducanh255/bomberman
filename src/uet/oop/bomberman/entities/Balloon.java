package uet.oop.bomberman.entities;

import java.util.Random;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import uet.oop.bomberman.BoardGame;
import uet.oop.bomberman.graphics.Sprite;

public class Balloon extends Entity {
    private static final String UP = "UP";
    private static final String DOWN = "DOWN";
    private static final String LEFT = "LEFT";
    private static final String RIGHT = "RIGHT";

    private static final int MAX_STEPS = 5;

    protected BoardGame boardGame;
    protected String direction;
    protected int steps;
    protected Timeline timeline = new Timeline();

    public Balloon(int xUnit, int yUnit, BoardGame boardGame) {
        super(xUnit, yUnit, Sprite.balloom_left1.getFxImage());
        this.boardGame = boardGame;
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1), e -> calculateMove()));
    }

    public void calculateMove() {
        Random random = new Random();
        int randint = random.nextInt(4);

        int dx = 0, dy = 0;
        if (randint == 0) dy--;
        if (randint == 2) dy++;
        if (randint == 1) dx++;
        if (randint == 3) dx--;

        int i = 0;
        if (i < MAX_STEPS) {
            move(dx * Sprite.SCALED_SIZE, dy * Sprite.SCALED_SIZE);
            i++;
        }
        i = 0;
    }

    public boolean canMove(int x_next, int y_next) {
        Entity entity = boardGame.getEnity(x_next, y_next);
        return entity == null;
    }

    public void move(int dx, int dy) {
        if (dy < 0) direction = UP;
        if (dy > 0) direction = DOWN;
        if (dx < 0) direction = LEFT;
        if (dx > 0) direction = RIGHT;

        if (canMove(x + dx, y + dy)) {
            x += dx;
            y += dy;
        }
    }

    @Override
    public void update() {
        timeline.play();
    }
}
