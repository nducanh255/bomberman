package uet.oop.bomberman.entities;

import java.sql.Time;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;
import uet.oop.bomberman.BoardGame;
import uet.oop.bomberman.graphics.Sprite;

public class Bomber extends Entity {
    private static final String UP = "UP";
    private static final String DOWN = "DOWN";
    private static final String LEFT = "LEFT";
    private static final String RIGHT = "RIGHT";

    protected String direction = "";
    protected BoardGame boardGame;

    public Bomber(int x, int y, BoardGame boardGame) {
        super(x, y, Sprite.player_dead1.getFxImage());
        this.boardGame = boardGame;
    }

    public void chooseSprite(String direction) {
        switch (this.direction) {
            case "UP":
                this.img = Sprite.player_up.getFxImage();
                break;
            case "DOWN":
                this.img = Sprite.player_down.getFxImage();
                break;
            case "LEFT":
                this.img = Sprite.player_left.getFxImage();
                break;
            case "RIGHT":
                this.img = Sprite.player_right.getFxImage();
                break;
            default:
                this.img = Sprite.player_right.getFxImage();
        }
    }

    public void calculateMove() {
        int dx = 0, dy = 0;
        if (boardGame.input == KeyCode.UP) dy--;
        if (boardGame.input == KeyCode.DOWN) dy++;
        if (boardGame.input == KeyCode.LEFT) dx--;
        if (boardGame.input == KeyCode.RIGHT) dx++;
        
        if (dx != 0 || dy != 0) {
            move(dx * Sprite.SCALED_SIZE, dy * Sprite.SCALED_SIZE);
            boardGame.input = null;
        }
    }

    public boolean canMove(int x_next, int y_next) {
        Entity entity = boardGame.getEnity(x_next, y_next);
        return entity == null;
    }
    
    public void move(int dx, int dy) {
        if (dy < 0) direction = UP;
        if (dy > 0) direction = DOWN;
        if (dx < 0) direction = LEFT;
        if (dx > 0) direction = RIGHT;

        if (canMove(x + dx, y + dy)) {
            x += dx;
            y += dy;
        }
    }

    public void createBomb() {
        if (this.boardGame.input == KeyCode.SPACE) {
            Bomb new_bomb = new Bomb(x, y);
            this.boardGame.entities.add(new_bomb);
            this.boardGame.bombs.add(new_bomb);
            this.boardGame.input = null;
        }
        return;
    }

    @Override
    public void update() {
        chooseSprite(this.direction);
        calculateMove();
        createBomb();
    }
}
