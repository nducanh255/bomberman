package uet.oop.bomberman.entities;

import javafx.animation.Timeline;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import uet.oop.bomberman.BoardGame;
import uet.oop.bomberman.graphics.Sprite;

public class Bomb extends Entity {

    public Bomb(int xUnit, int yUnit) {
        super(xUnit, yUnit, Sprite.bomb.getFxImage());
    }

    @Override
    public void update() {
    }

    public void render(GraphicsContext gc) {
        gc.drawImage(img, x, y);
    }
}
