package uet.oop.bomberman;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import uet.oop.bomberman.entities.Bomb;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.graphics.Sprite;

import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.List;

public class BombermanGame extends Application {
    public int WIDTH = 31;
    public int HEIGHT = 13;
    
    private GraphicsContext gc;
    private Canvas canvas;
    private BoardGame boardGame = new BoardGame();

    public static void main(String[] args) {
        Application.launch(BombermanGame.class);
    }

    @Override
    public void start(Stage stage) {
        // Tao Canvas
        canvas = new Canvas(Sprite.SCALED_SIZE * WIDTH, Sprite.SCALED_SIZE * HEIGHT);
        gc = canvas.getGraphicsContext2D();

        // Tao root container
        Group root = new Group();
        root.getChildren().add(canvas);

        // Tao scene
        Scene scene = new Scene(root);

        // Them scene vao stage
        stage.setScene(scene);
        stage.show();

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                render();
                update();
            }
        };
        timer.start();

        boardGame.createMap();

        scene.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            boardGame.input = e.getCode();
        });
    }

    public void update() {
        this.boardGame.bombs.forEach(g -> g.render(gc));
        this.boardGame.entities.forEach(Entity::update);
    }

    public void render() {
        gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        this.boardGame.stillObjects.forEach(g -> g.render(gc));
        this.boardGame.entities.forEach(g -> g.render(gc));
    }
}
