package uet.oop.bomberman;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javafx.beans.binding.ObjectExpression;
import javafx.scene.input.KeyCode;
import uet.oop.bomberman.entities.Balloon;
import uet.oop.bomberman.entities.Bomb;
import uet.oop.bomberman.entities.Bomber;
import uet.oop.bomberman.entities.Brick;
import uet.oop.bomberman.entities.Entity;
import uet.oop.bomberman.entities.Grass;
import uet.oop.bomberman.entities.Wall;
import uet.oop.bomberman.graphics.Sprite;

public class BoardGame {
    protected int width, height;
    public KeyCode input;
    public List<String> map = new ArrayList<>();
    public List<Entity> entities = new ArrayList<>();
    public List<Entity> stillObjects = new ArrayList<>();
    public List<Bomb> bombs = new ArrayList<>();

    public void createMap() {
        try {
            FileReader fr = new FileReader("res\\levels\\Level1.txt");
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (!line.equals("")) {
                map.add(line);
                line = br.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        map.remove(0);
        
        height = map.size();
        width = map.get(0).length();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                Entity object;
                switch (map.get(i).charAt(j)) {
                    case 'p':
                        object = new Bomber(j, i, this);
                        break;
                    case '#':
                        object = new Wall(j, i, Sprite.wall.getFxImage());
                        break;
                    case '*':
                        object = new Brick(j, i, Sprite.brick.getFxImage());
                        break;
                    case '1':
                        object = new Balloon(j, i, this);
                        break;
                    case '2':
                        object = new Brick(j, i, Sprite.oneal_left2.getFxImage());
                        break;
                    default:
                        object = new Grass(j, i, Sprite.grass.getFxImage());
                        break;
                }
                if (object instanceof Bomber  || object instanceof Balloon) {
                    entities.add(object);
                }
                else {
                    stillObjects.add(object);
                }
            }
        }
    }

    public Entity getEnity(int x, int y) {
        Entity res = null;
        for (Entity entity: stillObjects) {
            if (entity.x == x && entity.y == y && !(entity instanceof Grass)) {
                res = entity;
            }
        }
        return res;
    }
}
